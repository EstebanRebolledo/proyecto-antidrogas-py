# Generated by Django 2.2.7 on 2019-11-24 22:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0006_juego_respuesta'),
    ]

    operations = [
        migrations.AddField(
            model_name='juego',
            name='bueno',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='juego',
            name='malo',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='juego',
            name='medio',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
