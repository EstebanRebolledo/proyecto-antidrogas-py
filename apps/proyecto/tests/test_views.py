from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import User
from apps.proyecto import views
from mixer.backend.django import mixer
import pytest

@pytest.mark.django_db
class TestViews:
    def test_boletin_autenticacion(self):
        path = reverse('boletin')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)
        response = views.juego(request)
        assert response.status_code == 200
    
    def test_juego_autenticacion(self):
        path = reverse('thegame')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)
        response = views.juego(request)
        assert response.status_code == 200
    
    def test_tablero_puntuaciones_autenticacion(self):
        path = reverse('crono')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)
        response = views.juego(request)
        assert response.status_code == 200
    
    def test_contacto_autenticacion(self):
        path = reverse('contacto')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)
        response = views.juego(request)
        assert response.status_code == 200
    
    def test_index_autenticacion(self):
        path = reverse('index')
        request = RequestFactory().get(path)
        request.user = mixer.blend(User)
        response = views.juego(request)
        assert response.status_code == 200
    