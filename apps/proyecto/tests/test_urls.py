from django.urls import reverse,resolve


class TestUrls:
    def test_index_url(self):
        path = reverse('index')
        assert resolve(path).view_name == 'index'

    def test_juego_url(self):
        path = reverse('thegame')
        assert resolve(path).view_name == 'thegame'

    def test_boletin_url(self):
        path = reverse('boletin')
        assert resolve(path).view_name == 'boletin'

    def test_tableroPuntuacion_url(self):
        path = reverse('crono')
        assert resolve(path).view_name == 'crono'

    def test_login_url(self):
        path = reverse('login')
        assert resolve(path).view_name == 'login'

    def test_contacto_url(self):
        path = reverse('contacto')
        assert resolve(path).view_name == 'contacto'