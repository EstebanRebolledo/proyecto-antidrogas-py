from django.contrib import admin

from apps.proyecto.models import Usuario
from apps.proyecto.models import Contacto
from apps.proyecto.models import Boletin
from apps.proyecto.models import Donacion
from apps.proyecto.models import Invitado
from apps.proyecto.models import RegistroJuego
from apps.proyecto.models import Juego

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Contacto)
admin.site.register(Boletin)
admin.site.register(Donacion)
admin.site.register(Invitado)
admin.site.register(RegistroJuego)
admin.site.register(Juego)