from django.shortcuts import render
from django.contrib.auth import authenticate
from apps.proyecto.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from django.shortcuts import redirect
from apps.proyecto.forms import UserCreationForm
from apps.proyecto.models import Juego,RegistroJuego,Contacto
from .forms import PostForm,ContactoForm

# Create your views here.

def index(request):
    if request.user.is_authenticated:
        return render(request, "proyecto/index.html")
    return redirect('login')
def contacto(request):
    if request.user.is_authenticated:
        return render(request, 'proyecto/contacto.html')
    return redirect('login')
def donaciones(request):
    if request.user.is_authenticated:
        return render (request,'proyecto/donaciones.html')
    return redirect('login')
def boletin(request):
    if request.user.is_authenticated:
        return render (request,'proyecto/boletin.html')
    return redirect('login')
def ubicacion(request):
    if request.user.is_authenticated:
        return render (request, 'proyecto/ubicacion.html')
    return redirect('login')
def crono(request):
    if request.user.is_authenticated:
        registros = RegistroJuego.objects.all()
        return render(request, 'proyecto/crono.html',{'registros':registros})
    return redirect('login')
def MensajeEnviado(request):
    if request.user.is_authenticated:
        return render (request, 'proyecto/MensajeEnviado.html')
    return redirect('login')

    #LOGOUT

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('login')

    #LOGIN

def login(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect ('index')

    # Si llegamos al final renderizamos el formulario
    return render(request, "proyecto/login.html", {'form': form})

    #REGISTRO

def register(request):
    # Creamos el formulario de autenticación vacío
    form = UserCreationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = UserCreationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():

            # Creamos la nueva cuenta de usuario
            user = form.save()

            # Si el usuario se crea correctamente 
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "proyecto/registro.html", {'form': form})

def juego(request):
    
        if request.user.is_authenticated:
            if request.method == "POST":
                form = PostForm(request.POST)
                
                if form.is_valid():
                    post = form.save(commit=False)
                    post.save()
                    return redirect('/juego')
            else:
                juegos = Juego.objects.order_by("?").first()
                form = PostForm()
                return render(request, "proyecto/thegame.html",{'juego': juegos,'form': form})


        return redirect('login')


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

def contacto(request):
    
        if request.user.is_authenticated:
            if request.method == "POST":
                form = ContactoForm(request.POST)
                
                if form.is_valid():
                    post = form.save(commit=False)
                    post.save()
                    return redirect('/MensajeEnviado')
            else:
                contactos = Contacto.objects.order_by("?").first()
                form = ContactoForm()
                return render(request, "proyecto/contacto.html",{'contacto': contactos,'form': form})


        return redirect('MensajeEnviado')