﻿$(document).ready(function () {
    $("#formulario").validate({
        errorClass: 'is-invalid',
        rules: {
            nombre: {
                required: true
            },
            correo: {
                required: true,
                email: true
            },
            telefono: {
                required: true,
                number: true
            },
            asunto: {
                required: true,
            },
            mensaje: {
                required: true,
            },
            tipo: {
                required: true,
            },
            numerotarjeta: {
                required: true,
            },
            codigotarjeta: {
                required: true,
            },
            monto: {
                required: true,
            }
        },
        messages: {
            nombre: {
                required: "Se debe ingresar el nombre."
            },
            correo: {
                required: "Se debe ingresar un correo.",
                email: "Se debe ingresar un correo valido."
            },
            telefono: {
                required: "Se debe ingresar un numero de contacto.",
                number: "Porfavor ingrese un numero valido."
            },
            asunto: {
                required: "Se debe ingresar un asunto."
            },
            mensaje: {
                required: "Se debe ingresar un mensaje."
            },
            tipo: {
                required: "Seleccione un tipo de tarjeta."
            },
            numerotarjeta: {
                required: "Se debe ingresar el numero de la tarjeta."
            },
            codigotarjeta: {
                required: "Falta CVV."
            },
            monto: {
                required: "Se debe ingresar el monto."
            }


        }
    })

    $("#formulariodonacion").validate({
        errorClass: 'is-invalid',
        rules: {
            nombre: {
                required: true
            },
            correo: {
                required: true,
                email: true
            },
            telefono: {
                required: true,
                number: true
            },
            tipo: {
                required: true,
            },
            numerotarjeta: {
                required: true,
            },
            codigotarjeta: {
                required: true,
                maxlength: 3,
            },
            monto: {
                required: true,
            }
        },
        messages: {
            nombre: {
                required: "Se debe ingresar el nombre."
            },
            correo: {
                required: "Se debe ingresar un correo.",
                email: "Se debe ingresar un correo valido."
            },
            telefono: {
                required: "Se debe ingresar un numero de contacto.",
                number: "Porfavor ingrese un numero valido."
            },
            tipo: {
                required: "Seleccione un tipo de tarjeta."
            },
            numerotarjeta: {
                required: "Se debe ingresar el numero de la tarjeta."
            },
            codigotarjeta: {
                required: "Falta CVV.",
                maxlength: "CVV invalido"
            },
            monto: {
                required: "Se debe ingresar el monto."
            }


        }
    })

    $("#formularioboletin").validate({
        errorClass: 'is-invalid',
        rules: {
            nombrent: {
                required: true
            },
            correont: {
                required: true,
                email: true
            },
        },
        messages: {
            nombrent: {
                required: "Se debe ingresar el nombre."
            },
            correont: {
                required: "Se debe ingresar un correo.",
                email: "Se debe ingresar un correo valido."
            },

        }

    })

    $("#formulariologin").validate({
        errorClass: 'is-invalid',
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            },
        },
        messages: {
            username: {
                required: "Se debe ingresar el usuario.",
            },
            password: {
                required: "Se debe ingresar la contraseña."
            },   
        }
    })

    $("#formularioregistro").validate({
        errorClass: 'is-invalid',
        rules: {
            username: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password1: {
                required: true
            },
            password2: {
                required: true
            }
        },
        messages: {
            username: {
                required: "Se debe ingresar un usuario.",
            },
            email: {
                required: "Se debe ingresar un correo.",
                email: "Se debe ingresar un correo valido."
            },
            password1: {
                required: "Se debe ingresar una contraseña."
            },
            password2: {
                required: "Se debe ingresar una contraseña."
            }
        }
    })
}) 

$("#formularioreset").validate({
    errorClass: 'is-invalid',
    rules: {
        email: {
            required: true,
            email: true
        },
    },
    messages: {
        email: {
            required: "Se debe ingesar un correo.",
            email: "Ingrese un correo valido"
        },
    }
})

$("#formularioreset2").validate({
    errorClass: 'is-invalid',
    rules: {
        new_password1: {
            required: true,
        },
        new_password2: {
            required: true,
        },
    },
    messages: {
        new_password1: {
            required: "Ingrese una contraseña"
        },
        new_password2: {
            required: "Ingrese una contraseña"
        },
    }
})


function RegJug(){
    var var1 = $('#respuesta').val();
    var var2 = $.trim($('#resp').html());
    if(var1 ==var2){
        $('#marcas').attr('hidden',false);
        //llenas inputs
        $('#id_usuario').val($.trim($('#username').html()));
        $('#id_tiempo').val($('#crono').html());
        $('#id_Juego').val($.trim($('#idJuego').html()));
        //Generar estado drug
        var estadodrug = "";
        var min = $.trim($('#bueno').html());
        var med = $.trim($('#medio').html());
        var max = $.trim($('#malo').html());
        var tiempo = $('#crono').html();
        var min2= min.split(':');
        var med2 = med.split(':');
        var max2 = max.split(':');
        tiempo=tiempo.split(':');
        var min3=( (+min2[0]) * 60 + (+min2[1]));
        var med3=((+med2[0]) * 60 + (+med2[1]));
        var max3=((+max2[0]) * 60 + (+max2[1]));
        tiempo=(+tiempo[0]) * 60 * 60 + (+tiempo[1]) * 60 + (+tiempo[2]);
        if(tiempo<min3){
            /*
            var apodos= ["Genio", "Crack", "Idolo", "Titan", "Bestia",  "Rey", "Superheroe", "Locura", "Gladiador", "Pantera", "Lince"];
            var apodo= apodos[Math.floor(Math.random() * apodos.length)];
            */

            estadodrug=$('#apodo').html();
        }else if(tiempo<med3){
            estadodrug="Lucido";
        }else if(tiempo<max3){
            estadodrug="Consumidor social";
        }else{
            estadodrug="Ignacio Gomez";
        }
        $('#id_estadodrug').val(estadodrug);
        document.getElementById('ejemplo').innerHTML='Respuesta Correcta en '+$('#crono').html()+' Tu estado es: '+estadodrug;
        //deshabilitar inputs
        $('#id_usuario').attr('hidden',true);
        $('#id_tiempo').attr('hidden',true);
        $('#id_estadodrug').attr('hidden',true);
        $('#id_Juego').attr('hidden',true);
        //
        $('#marcador').attr('hidden',false);
        empezarDetener(document.getElementById('btn_crono'));
        $('#RegistrarJugada').attr('hidden',true);

    }
    else
        document.getElementById('ejemplo').innerHTML='Respuesta Incorrecta';
}



//#region KonamiCode
const pressed = [];
const secretCode = 'asies';

window.addEventListener('keyup', (e) => {
    console.log(e.key);
    pressed.push(e.key);
    pressed.splice(-secretCode.length - 1, pressed.length - secretCode.length);
    if (pressed.join('').includes(secretCode)) {
        document.body.style.backgroundImage = "url('proyecto/img/rixar.JPG')";
        console.log('Cursed');

    }
    console.log(pressed);
})

src = "https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"
src = "http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"
$(document).ready(function () {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
});

$(function () {
    $(window).jKonamicode(function () {
        document.body.style.backgroundImage = "url('https://live.staticflickr.com/65535/49124133857_d6451b3768_z.jpg')";
        alert("Totally cursed!");
        $('.cursed').attr('src','https://live.staticflickr.com/65535/49124133857_d6451b3768_z.jpg');
    });
});

//#endregion

//#region TIMER
var inicio=0;
var timeout=0;

function empezarDetener(elemento)
{
    if(timeout==0)
    {
        // empezar el cronometro

        elemento.value="Detener";

        // Obtenemos el valor actual
        inicio=vuelta=new Date().getTime();

        // iniciamos el proceso
        $(".se-pre-con2").fadeOut("slow");
        var audio = new Audio("static/proyecto/img/kahoot.mp3");
        audio.volume=0.2;
        audio.play();
        var apodos= ['Genio', 'Crack', 'Idolo', 'Titan', 'Bestia',  'Rey', 'Superheroe', 'Locura', 'Gladiador', 'Pantera', 'Lince'];
        var apodo= apodos[Math.floor(Math.random() * apodos.length)];
        document.getElementById('apodo').innerHTML=apodo.trim();
        var rescate = document.getElementById('buenpuntaje').innerHTML;
        document.getElementById('buenpuntaje').innerHTML='"'+$('#apodo').html()+'" '+rescate;
        funcionando();
    }else{
        // detemer el cronometro

        elemento.value="Empezar";
        clearTimeout(timeout);
        timeout=0;
    }
}

function funcionando()
{
    // obteneos la fecha actual
    var actual = new Date().getTime();

    // obtenemos la diferencia entre la fecha actual y la de inicio
    var diff=new Date(actual-inicio);

    // mostramos la diferencia entre la fecha actual y la inicial
    var result=LeadingZero(diff.getUTCHours())+":"+LeadingZero(diff.getUTCMinutes())+":"+LeadingZero(diff.getUTCSeconds());
    document.getElementById('crono').innerHTML = result;

    // Indicamos que se ejecute esta función nuevamente dentro de 1 segundo
    timeout=setTimeout("funcionando()",1000);
}

/* Funcion que pone un 0 delante de un valor si es necesario */
function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : + Time;
}
//#endregion

var map = L.map('map').setView([-33.455285, -71.663283], 17);

L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">StayClean</a>',
	subdomains: 'abcd',
	maxZoom: 19
}).addTo(map);

var marcador = L.marker([-33.455285, -71.663283]).addTo(map);
