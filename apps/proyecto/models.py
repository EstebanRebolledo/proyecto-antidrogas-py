from django.db import models

# Create your models here.

class Usuario(models.Model):
    nombre = models.CharField(max_length=12, primary_key=True)
    contraseña = models.CharField(max_length=18)
    correo = models.EmailField()
    fechanacimiento = models.DateField()
    sexo = models.CharField(max_length=1)

class Boletin(models.Model):
    nombre = models.CharField(max_length=50)
    correo = models.EmailField()

class Invitado(models.Model):
    rut = models.IntegerField()
    puntaje = models.IntegerField()




class Donacion(models.Model):
    nombre = models.CharField(max_length=12)
    correo = models.EmailField()
    telefono = models.CharField(max_length=12)
    tipotarjeta = models.CharField(max_length=10)
    numerotarjeta = models.IntegerField()
    cvv = models.IntegerField()
    monto = models.IntegerField()

class Contacto(models.Model):
    nombre = models.CharField(max_length=12)
    correo = models.EmailField()
    telefono = models.CharField(max_length=12)
    asunto = models.CharField(max_length=90)
    mensaje = models.CharField(max_length=500)
    

class Juego(models.Model):
    id_juego = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=12)
    pregunta = models.CharField(max_length=200)
    respuesta = models.CharField(max_length=200)
    img = models.CharField(max_length=200)
    bueno = models.CharField(max_length=12)
    medio = models.CharField(max_length=12)
    malo = models.CharField(max_length=12)

class RegistroJuego(models.Model):
    usuario = models.CharField(max_length=20)
    tiempo = models.CharField(max_length=20)
    estadodrug = models.CharField(max_length=30)
    Juego = models.ForeignKey(Juego,  on_delete=models.CASCADE)