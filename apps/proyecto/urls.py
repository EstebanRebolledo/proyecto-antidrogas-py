from django.contrib import admin
from django.urls import path, re_path
from .views import index
from .views import contacto
from .views import donaciones
from .views import ubicacion
from .views import boletin
from .views import login
from .views import crono
from .views import logout
from .views import register
from .views import juego
from .views import post_new
from .views import MensajeEnviado
#from .views import recovery
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView, PasswordResetDoneView, PasswordResetCompleteView



urlpatterns = [
    path ('',index, name="index"),
    path ('contacto',contacto, name="contacto"),
    path ('ubicacion',ubicacion, name="ubicacion"),
    path ('donaciones',donaciones, name="donaciones"),
    path ('boletin',boletin, name="boletin"),
    path('login', login, name="login"),
    path('crono',crono, name="crono"),
    path('MensajeEnviado',MensajeEnviado, name="MensajeEnviado"),

    path('logout',logout, name='logout'),
    path('register',register, name='register'),
    path('juego',juego, name='thegame'),
    path('recuperarcontrasena', PasswordResetView.as_view(template_name='proyecto/recuperarpass/password_reset_form.html', email_template_name="proyecto/recuperarpass/password_reset_email.html"), name = 'password_reset'),
    path('recuperarcontrasena/enviado', PasswordResetDoneView.as_view(template_name='proyecto/recuperarpass/password_reset_done.html'), name = 'password_reset_done'),
    re_path(r'^recuperarcontrasena(?P<uidb64>[0-9A-za-z_\-]+)/(?P<token>.+)/$', PasswordResetConfirmView.as_view(template_name='proyecto/recuperarpass/password_reset_confirm.html'), name = 'password_reset_confirm'),
    path('recuperarcontrasena/exito',PasswordResetCompleteView.as_view(template_name='proyecto/recuperarpass/password_reset_complete.html') , name = 'password_reset_complete'),
    path('post/new',post_new, name='post_new'),

    #path('recovery',recovery, name='recovery'),
]