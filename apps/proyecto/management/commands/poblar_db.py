from django.core.management.base import BaseCommand
from apps.proyecto.models import *

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def poblar(self):
        j1 = Juego()
        j1.id_juego=1
        j1.nombre="CUATRO DIGITOS!"
        j1.pregunta="Encuentra los valores de A, B, C y D teniendo en cuenta las ecuaciones mostradas en pantalla."
        j1.respuesta="2368"
        j1.img="https://live.staticflickr.com/65535/49111920753_07daeabb6a_b.jpg"
        j1.bueno="3:00"
        j1.medio="5:30"
        j1.malo="8:00"
        j1.save()

        j2 = Juego()
        j2.id_juego=2
        j2.nombre="GATOS Y RATONES"
        j2.pregunta="Cinco gatos pueden cazar cinco ratones en cinco minutos ¿Cuántos gatos se necesitan para cazar 100 ratones en 100 minutos?"
        j2.respuesta="5"
        j2.img="https://live.staticflickr.com/65535/49112033118_7acdf6776a_b.jpg"
        j2.bueno="2:30"
        j2.medio="5:00"
        j2.malo="7:30"
        j2.save()

        j3 = Juego()
        j3.id_juego=3
        j3.nombre="MANECILLAS"
        j3.pregunta="¿Cuántas veces se cruzan las manecillas de un reloj analógico entre las 12 del mediodía y las doce de la noche?"
        j3.respuesta="10"
        j3.img="https://live.staticflickr.com/65535/49117866613_6db28fb0f2_b.jpg"
        j3.bueno="5:00"
        j3.medio="7:30"
        j3.malo="10:00"
        j3.save()

        j4 = Juego()
        j4.id_juego=4
        j4.nombre="TRABAJO EN LA GRANJA"
        j4.pregunta="Alfred y Roland deben arar un terreno de 4 hectáreas por el que se pagan $100. Teniendo en cuenta que dividen el terreno por la mitad para trabajar cada uno en un lado, que Alfred trabaja a un ritmo de 20 minutos por hectárea, y que Roland tarda 40 minutos, pero lo hace tres veces más rápido que Alfred. ¿Cuánto dinero ganará Roland?"
        j4.respuesta="50"
        j4.img="https://live.staticflickr.com/65535/49117866443_708a4595cd_b.jpg"
        j4.bueno="3:00"
        j4.medio="5:30"
        j4.malo="8:00"
        j4.save()

        j5 = Juego()
        j5.id_juego=3
        j5.nombre="ARCO Y LÍNEA"
        j5.pregunta="¿Cuál es la longitud de la diagonal que va de A a C? (En Centímetros)"
        j5.respuesta="10"
        j5.img="https://live.staticflickr.com/65535/49118372351_4f336dd35d_b.jpg"
        j5.bueno="3:30"
        j5.medio="5:30"
        j5.malo="9:00"
        j5.save()

        j6 = Juego()
        j6.id_juego=6
        j6.nombre="¡SÁLVESE QUIEN PUEDA!"
        j6.pregunta="Hay 15 personas atrapadas en un barco que se va hundir dentro de 20 minutos. Disponen de un bote en el que caben 5 personas, y un viaje de ida y vuelta a tierra firme dura 9 minutos ¿cuantas personas podrán salvarse?"
        j6.respuesta="13"
        j6.img="https://live.staticflickr.com/65535/49117866588_8dbfeff5df_b.jpg"
        j6.bueno="3:00"
        j6.medio="5:30"
        j6.malo="8:00"
        j6.save()

        j7 = Juego()
        j7.id_juego=7
        j7.nombre="TRIÁNGULOS"
        j7.pregunta="¿Cuántas veces tienes que meter la pluma en el tintero para dibujar la figura grande de 36 triángulos?"
        j7.respuesta="7"
        j7.img="https://live.staticflickr.com/65535/49117866628_d9fe6ef738_b.jpg"
        j7.bueno="4:00"
        j7.medio="6:30"
        j7.malo="9:00"
        j7.save()

        j8 = Juego()
        j8.id_juego=8
        j8.nombre="UN TARRO DE VIRUS"
        j8.pregunta="Un virus llena un tarro en una hora, dividiéndose en dos una vez al minuto ¿Cuántos minutos tardará el tarro en llenarse si empezaras con dos virus?"
        j8.respuesta="59"
        j8.img="https://live.staticflickr.com/65535/49118372381_e80f2e86d3_b.jpg"
        j8.bueno="3:00"
        j8.medio="4:00"
        j8.malo="7:00"
        j8.save()

        j9 = Juego()
        j9.id_juego=9
        j9.nombre="TIPAZO"
        j9.pregunta="Es Acaso este un Tipazo? \n 'si' o 'no' "
        j9.respuesta="si"
        j9.img="https://live.staticflickr.com/65535/49118703722_dc7514ddf1_b.jpg"
        j9.bueno="2:00"
        j9.medio="4:00"
        j9.malo="6:00"
        j9.save()

        j10 = Juego()
        j10.id_juego=10
        j10.nombre="PUZLE EQUINO"
        j10.pregunta="Tres caballos pueden dar 2, 3 y 4 vueltas respectivamente en un minuto. Si todos empiezan a correr a la vez en la misma dirección ¿Cuántos minutos pasarán antes de que vuelvan a coincidir en la línea de salida?"
        j10.respuesta="A"
        j10.img="https://live.staticflickr.com/65535/49118372096_9ae280e5e7_b.jpg"
        j10.bueno="3:00"
        j10.medio="5:00"
        j10.malo="8:00"
        j10.save()

        j11 = Juego()
        j11.id_juego=11
        j11.nombre="DEMASIADOS RATONES"
        j11.pregunta="El tipo de ratón que tenemos aquí puede parir 12 bebés al mes. A su vez, sus crías pueden tener sus propios bebés a los 2 meses de nacer. Si compras una ratoncita de ese tipo y te la llevas a casa ¿Cuántos ratones tendrás dentro de 10 meses?"
        j11.respuesta="1"
        j11.img="https://live.staticflickr.com/65535/49118372251_153e1766d8_b.jpg"
        j11.bueno="3:10"
        j11.medio="5:30"
        j11.malo="8:25"
        j11.save()

        j12 = Juego()
        j12.id_juego=12
        j12.nombre="¿CUÁNTOS AÑOS?"
        j12.pregunta="Si un hijo tiene 22 años, y su padre tiene su edad, más la mitad de la suya ¿Cuántos años tiene el padre?"
        j12.respuesta="44"
        j12.img="https://live.staticflickr.com/65535/49117866168_3b6d9489a3_b.jpg"
        j12.bueno="3:00"
        j12.medio="5:00"
        j12.malo="8:00"
        j12.save()

        j13 = Juego()
        j13.id_juego=13
        j13.nombre="¿Y LA MADRE?"
        j13.pregunta="Hubo un tiempo en que un hombre tenía el doble de la edad de su mujer. Por supuesto, al año siguiente, sólo tenía la edad de la mujer más la mitad ¿Cuántos años tiene la mujer, teniendo en cuenta que el hombre tiene ahora 44 años?"
        j13.respuesta="43"
        j13.img="https://live.staticflickr.com/65535/49118372306_31e8aff619_b.jpg"
        j13.bueno="4:00"
        j13.medio="7:00"
        j13.malo="11:00"
        j13.save()

        j14 = Juego()
        j14.id_juego=14
        j14.nombre="ECUACIONES RARAS"
        j14.pregunta="Si 8 - 6 = 2, y 8 + 6 = 2 ¿A qué es igual siete más seis?"
        j14.respuesta="1"
        j14.img="https://live.staticflickr.com/65535/49117866223_61834b8621_b.jpg"
        j14.bueno="4:20"
        j14.medio="6:09"
        j14.malo="8:20"
        j14.save()

        j15 = Juego()
        j15.id_juego=15
        j15.nombre="TREN DESBOCADO"
        j15.pregunta="Un tren de 100 m de longitud tarda 30 segundos en recorrer un puente de 400 m. Si su velocidad es constante ¿A qué velocidad (en km/h) se desplaza este tren?"
        j15.respuesta="60"
        j15.img="https://live.staticflickr.com/65535/49118567007_d3f00fb8ec_b.jpg"
        j15.bueno="5:00"
        j15.medio="7:11"
        j15.malo="9:45"
        j15.save()

        j16 = Juego()
        j16.id_juego=16
        j16.nombre="DEPÓSITO DE AGUA"
        j16.pregunta="Un depósito tiene 2,5 m de profundidad, y se llena de agua durante ocho horas, a partir de las 9:00. En ese tiempo, el nivel sube 60 cm, pero parece que hay una fuga, y por la noche el nivel baja 20 cm. Si el nivel sigue subiendo 40 cm al día, ¿cuántos días tardará en desbordarse el depósito?"
        j16.respuesta="6"
        j16.img="https://live.staticflickr.com/65535/49117866313_5daa67cf59_b.jpg"
        j16.bueno="7:00"
        j16.medio="10:00"
        j16.malo="12:00"
        j16.save()

        j17 = Juego()
        j17.id_juego=17
        j17.nombre="A LA DERIVA"
        j17.pregunta="Si el agua del mar alcanza el noveno escalón de una escalera que cuelga de un barco. La marea sube 40 cm cada hora, y hay un espacio de 30 cm entre cada escalón, ¿a qué escalón habrá llegado el agua al cabo de tres horas?"
        j17.respuesta="9"
        j17.img="https://live.staticflickr.com/65535/49118567037_8102b89b44_b.jpg"
        j17.bueno="5:00"
        j17.medio="8:00"
        j17.malo="11:00"
        j17.save()

        j18 = Juego()
        j18.id_juego=18
        j18.nombre="CÁLCULOS ARCANOS"
        j18.pregunta="Si en un dado uno equivale a 15, y seis a 20 ¿a qué corresponderá si sale tres?"
        j18.respuesta="17"
        j18.img="https://live.staticflickr.com/65535/49117866358_bb56015182_b.jpg"
        j18.bueno="6:00"
        j18.medio="9:00"
        j18.malo="12:00"
        j18.save()

        rj1 = RegistroJuego()
        rj1.usuario="esteban"
        rj1.tiempo="00:06:00"
        rj1.estadodrug="Consumidor Social"
        rj1.Juego = j1
        rj1.save()
        
        rj2 = RegistroJuego()
        rj2.usuario="esteban"
        rj2.tiempo="00:00:26"
        rj2.estadodrug="Genio"
        rj2.Juego = j4
        rj2.save()

        rj3 = RegistroJuego()
        rj3.usuario="esteban"
        rj3.tiempo="00:04:55"
        rj3.estadodrug="Lúcido"
        rj3.Juego = j5
        rj3.save()

        rj4 = RegistroJuego()
        rj4.usuario="esteban"
        rj4.tiempo="00:08:13"
        rj4.estadodrug="Ignacio Gomez"
        rj4.Juego = j6
        rj4.save()

        rj5 = RegistroJuego()
        rj5.usuario="esteban"
        rj5.tiempo="00:08:55"
        rj5.estadodrug="Ignacio Gomez"
        rj5.Juego = j8
        rj5.save()

        rj6 = RegistroJuego()
        rj6.usuario="esteban"
        rj6.tiempo="00:07:43"
        rj6.estadodrug="Consumidor Social"
        rj6.Juego = j13
        rj6.save()

        print(" __      __")
        print("( _\    /_ )")
        print(" \ _\  /_ / ")
        print("  \ _\/_ /_ _ ")
        print("  |_____/_/ /|")
        print("  (  (_)__)J-)")
        print("  (  /`.,   /")
        print("   \/  ;   /")
        print("    | === |")
        
    def handle(self, *args, **options):
        self.poblar()

